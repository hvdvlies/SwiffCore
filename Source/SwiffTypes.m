//
//  SwiffTypes.m
//  SwiffCore
//
//  Created by Ricci Adams on 2012-01-06.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SwiffTypes.h"

const SwiffColorTransform SwiffColorTransformIdentity = {
    1.0, 1.0, 1.0, 1.0,
    0.0, 0.0, 0.0, 0.0
};
